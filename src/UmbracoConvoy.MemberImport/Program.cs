﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UmbracoConvoy.File;

namespace UmbracoConvoy.MemberImport
{
    class Program
    {
        class Record
        {
            public string Polref { get; set; }

            [ColumnProperty("i_urn")]
            public string Urn { get; set; }

            [ColumnProperty("date_of_birth", 0L, "yyyy-MM-dd")]
            public DateTime DateOfBirth { get; set; }

            [ColumnProperty("record_extract_date", 0L, "ddMMMyyyy")]
            public DateTime ExtractDate { get; set; }

        }

        static bool Process<Record>(string identifier, Record record)
        {
            return true;
        }

        static void Main(string[] args)
        {
            var access = new UmbracoConvoy.Services.Access("server=.;database=UmbracoConvoy;user id=umbraco;password=umbraco");

            var file = new File.Reader();

            file.Read<Record>(args[0], true, Process);



            //access.UpgradeSchemaAndData();
        }
    }
}
