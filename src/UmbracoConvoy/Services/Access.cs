﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Reflection;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace UmbracoConvoy.Services
{
    public class Access
    {
        public string ConnectionString { get; internal set; }
        public ServiceContext Services { get; internal set; }

        private void InitConfig()
        {
            if (!ValidConfig())
            {
                using (var exeConfiguration = new Config.Models.ExeConfiguration())
                {
                    exeConfiguration.CreateSection("clientDependency", "ClientDependency.Core.Config.ClientDependencySection, ClientDependency.Core", false, 
                        "ClientDependency.config", Config.EmbeddedFiles.ClientDependency);

                    exeConfiguration.CreateSection("Examine", "Examine.Config.ExamineSettings, Examine", false, 
                        "ExamineSettings.config", Config.EmbeddedFiles.ExamineSettings);

                    exeConfiguration.CreateSection("ExamineLuceneIndexSets", "Examine.LuceneEngine.Config.IndexSets, Examine", false, 
                        "ExamineIndex.config", Config.EmbeddedFiles.ExamineIndex);

                    exeConfiguration.CreateSection("log4net", "log4net.Config.Log4NetConfigurationSectionHandler, log4net", false, 
                        "log4net.config", Config.EmbeddedFiles.log4net);

                    exeConfiguration.RemoveSectionGroup("umbracoConfiguration");

                    exeConfiguration.CreateSectionGroup("umbracoConfiguration", "settings", "Umbraco.Core.Configuration.UmbracoSettings.UmbracoSettingsSection, Umbraco.Core", false, 
                        "umbracoSettings.config", Config.EmbeddedFiles.umbracoSettings);

                    exeConfiguration.CreateSectionGroup("umbracoConfiguration", "BaseRestExtensions", "Umbraco.Core.Configuration.BaseRest.BaseRestSection, Umbraco.Core", false, 
                        "BaseRestExtensions.config", Config.EmbeddedFiles.BaseRestExtensions);

                    exeConfiguration.CreateSectionGroup("umbracoConfiguration", "FileSystemProviders", "Umbraco.Core.Configuration.FileSystemProvidersSection, Umbraco.Core", false, 
                        "FileSystemProviders.config", Config.EmbeddedFiles.FileSystemProviders);

                    exeConfiguration.CreateSectionGroup("umbracoConfiguration", "dashBoard", "Umbraco.Core.Configuration.Dashboard.DashboardSection, Umbraco.Core", false, 
                        "Dashboard.config", Config.EmbeddedFiles.Dashboard);
                }
                if (!ValidConfig())
                {
                    System.Environment.Exit(-1);        //  We can't update ourselves, bug in .net when trying to reload config files that we are using ourselves
                }
            }

        }

        private void InitServices()
        {
            var AppBase = new StandaloneBase();
            if (ApplicationContext.Current == null)
                AppBase.Start(AppBase, new EventArgs());
            var AppContext = ApplicationContext.Current;
            var DatabaseContext = AppContext.DatabaseContext;
            Services = AppContext.Services;
        }

        private bool ValidConfig()
        {
            object umbracoSettings = null;
            object baseRestExtensions = null;
            object dashboardConfig = null;

            try
            {
                umbracoSettings = System.Configuration.ConfigurationManager.GetSection("umbracoConfiguration/settings");
                baseRestExtensions = System.Configuration.ConfigurationManager.GetSection("umbracoConfiguration/BaseRestExtensions");              
                dashboardConfig = System.Configuration.ConfigurationManager.GetSection("umbracoConfiguration/dashBoard");                
            }
            catch (Exception ex)
            {
                //  Swallow everything
            }

            return (umbracoSettings != null && baseRestExtensions != null && dashboardConfig != null) ? true : false;
        }

        public Access() 
        {
            ConnectionString = Config.ConnectionString.Umbraco;

            InitConfig();
            InitServices();
        }

        public Access(string connectionString)
        {
            string currentConnectionString = null;
            try
            {
                currentConnectionString = Config.ConnectionString.Umbraco;
            }
            catch (ConfigurationErrorsException)
            {
                //  Swallow, as this means the connection string doesn't exist
            }
            if (currentConnectionString != connectionString)
            {
                Config.ConnectionString.Umbraco = connectionString;
            }
            ConnectionString = connectionString;

            InitConfig();
            InitServices();

            if (currentConnectionString != connectionString)
            {
                Config.ConnectionString.Umbraco = currentConnectionString ?? "";
            }
        }

        public bool UpgradeSchemaAndData()
        {
            MethodInfo dynMethod = ApplicationContext.Current.DatabaseContext.GetType().GetMethod("UpgradeSchemaAndData", BindingFlags.NonPublic | BindingFlags.Instance);
            var result = dynMethod.Invoke(ApplicationContext.Current.DatabaseContext, new IMigrationEntryService[] { Services.MigrationEntryService });
            if (result == null)
            {
                return false;
            }

            return (bool) result.GetType().GetProperty("Success").GetValue(result);
        }

    }
}
