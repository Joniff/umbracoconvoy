﻿using System;
using Umbraco.Core;

namespace UmbracoConvoy.Services
{
    internal class StandaloneBase : UmbracoApplicationBase
    {
        protected override IBootManager GetBootManager()
        {
            return new BootManager(this);
        }

        public void Start(object Sender, EventArgs Args)
        {
            Application_Start(Sender, Args);
        }
    }
}