﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UmbracoConvoy.Database
{
    [AttributeUsage(AttributeTargets.All)]
    public class UmbracoPropertyAttribute : System.Attribute
    {
        public enum UmbracoPropertyType
        {
            Auto = 0,       //  Try and work out property type from context
            String = 1,     //  Either ntext or nvarchar
            DateTime = 2,
            Integer = 3,
            NText = 4,
            NVarChar = 5
        };
        
        public string Alias;
        public UmbracoPropertyType PropertyType;

        public UmbracoPropertyAttribute(string alias, UmbracoPropertyType propertyType = UmbracoPropertyType.Auto)
        {
            Alias = alias;
            PropertyType = propertyType;
        }
    }
}
