﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UmbracoConvoy.Database
{
    public class Content
    {
        public string ConnectionString { get; internal set; }
        private int ConnectionTimeOutInSeconds = 4 * 60;      //  How long do we allow sql scripts to run on server, before we kill them
        
        private void Init()
        {
            string sql = @"
                IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'UmbracoConvoyDatabaseIndex1' AND object_id = OBJECT_ID('cmsPropertyData'))
                BEGIN
	                CREATE NONCLUSTERED INDEX [UmbracoConvoyDatabaseIndex1] ON [cmsPropertyData]
	                (
		                [contentNodeId] ASC,
		                [propertytypeid] ASC,
		                [versionId] ASC,
		                [id] ASC
	                )
	                INCLUDE ( 	[dataNvarchar]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
                END";
            
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(sql.ToString(), connection))
                {
                    command.CommandTimeout = ConnectionTimeOutInSeconds;
                    command.ExecuteNonQuery();
                }
            }
        }

        public Content()
        {
            ConnectionString = Config.ConnectionString.Umbraco;
            Init();
        }

        public Content(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentNullException();
            }
            ConnectionString = connectionString;
            Init();
        }

        public Content(int connectionTimeOutInSeconds)
        {
            ConnectionTimeOutInSeconds = connectionTimeOutInSeconds;
            Init();
        }

        public Content(string connectionString, int connectionTimeOutInSeconds)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentNullException();
            }
            ConnectionString = Config.ConnectionString.Umbraco;
            ConnectionTimeOutInSeconds = connectionTimeOutInSeconds;
            Init();
        }

        private string MapPropertyDataField<T>(string propertyAlias, string tableAlias)
        {
            if (typeof(T) == typeof(string))
                return string.Format("isnull([{0}].[dataNvarchar], convert(nvarchar(max),[{0}].[dataNtext]))", tableAlias);
            else if (typeof(T) == typeof(DateTime))
                return tableAlias + ".[dataDate]";
            else if (typeof(T) == typeof(int))
                return tableAlias + ".[dataInt]";
            else
                throw new ArgumentException("Generic type has to be of type string, datetime or int");
        }

        private object NullField<T>()
        {
            if (typeof(T) == typeof(string))
                return string.Empty;
            else if (typeof(T) == typeof(DateTime))
                return DateTime.MinValue;
            else if (typeof(T) == typeof(int))
                return 0;
            else
                throw new ArgumentException("Generic type has to be of type string, datetime or int");
        }

        private string SqlJoinPropertyData(string tableAlias, string propertyAlias)
        {
            return string.Format(" join [cmsPropertyData] as {0} on [{0}].[versionId] = [d].[versionId] and [{0}].[propertytypeid] = (select [id] from [cmsPropertyType] where [Alias] = '{1}' and [contentTypeId] = [t].[nodeId])", tableAlias, propertyAlias);
        }

        public IDictionary<int, string> GetPublishedNodes(string docTypeAlias = null, int ancestorId = -1, bool ignoreDuplicates = false)
        {
            return GetPublishedNodesOfProperties<int, string>(docTypeAlias, ancestorId, ignoreDuplicates, BuiltInPropertyAlias.Id, BuiltInPropertyAlias.Name);
        }

        public IList<T> GetPublishedNodesOfProperty<T>(string docTypeAlias = null, int ancestorId = -1, 
            string propertyAlias = BuiltInPropertyAlias.Id)
        {
            var records = new List<T>();
            
            var sql = new StringBuilder();
            bool builtInPropertyAlias = BuiltInPropertyAlias.IsBuiltInPropertyAlias(propertyAlias);

            sql.Append("select ");
            if (builtInPropertyAlias)
                sql.Append(BuiltInPropertyAlias.Map(propertyAlias));
            else
                sql.Append(MapPropertyDataField<T>(propertyAlias, "pd1"));

            sql.Append(@" as [key] from [umbracoNode] as [n]
                        join [cmsDocument] as [d] on
		                    [d].[nodeId] = [n].[id]
	                    join [cmsContent] as [c] on
		                    [c].[nodeId] = [d].[nodeId]
	                    join [cmsContentType] as [t] on
		                    [t].[nodeId] = [c].[contentType]");

            if (!builtInPropertyAlias)
            {
                sql.Append(SqlJoinPropertyData("pd1", propertyAlias));
            }

            sql.Append(" where ");
            if (!string.IsNullOrWhiteSpace(docTypeAlias))
            {
                sql.Append("[t].[alias] = '");
                sql.Append(docTypeAlias);
                sql.Append("' and ");
            }
            sql.Append("[d].[published] = 1 and [n].[path] like '");
            if (ancestorId != -1)
            {
                sql.Append("%,");
            }
            sql.Append(ancestorId);
            sql.Append(",%'");

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(sql.ToString(), connection))
                {
                    command.CommandTimeout = ConnectionTimeOutInSeconds;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                                records.Add(reader.GetFieldValue<T>(0));
                        }
                    }
                }
            }
            return records;
        }

        public IDictionary<K, V> GetPublishedNodesOfProperties<K, V>(string docTypeAlias = null, int ancestorId = -1, bool ignoreDuplicates = false,
            string keyPropertyAlias = BuiltInPropertyAlias.Id,
            string valuePropertyAlias = BuiltInPropertyAlias.Name)
        {
            var records = new Dictionary<K, V>();

            var sql = new StringBuilder();
            bool builtInPropertyAlias1 = BuiltInPropertyAlias.IsBuiltInPropertyAlias(keyPropertyAlias);
            bool builtInPropertyAlias2 = BuiltInPropertyAlias.IsBuiltInPropertyAlias(valuePropertyAlias);

            sql.Append("select ");
            if (builtInPropertyAlias1)
                sql.Append(BuiltInPropertyAlias.Map(keyPropertyAlias));
            else
                sql.Append(MapPropertyDataField<K>(keyPropertyAlias, "pd1"));

            sql.Append(" as [key], ");
            if (builtInPropertyAlias2)
                sql.Append(BuiltInPropertyAlias.Map(valuePropertyAlias));
            else
                sql.Append(MapPropertyDataField<V>(valuePropertyAlias, "pd2"));
            
            sql.Append(@" as [value] from [umbracoNode] as [n]
                        join [cmsDocument] as [d] on
		                    [d].[nodeId] = [n].[id]
	                    join [cmsContent] as [c] on
		                    [c].[nodeId] = [d].[nodeId]
	                    join [cmsContentType] as [t] on
		                    [t].[nodeId] = [c].[contentType]");

            if (!builtInPropertyAlias1)
            {
                sql.Append(SqlJoinPropertyData("pd1", keyPropertyAlias));
            }
            if (!builtInPropertyAlias2)
            {
                sql.Append(SqlJoinPropertyData("pd2", valuePropertyAlias));
            }

            sql.Append(" where ");
            if (!string.IsNullOrWhiteSpace(docTypeAlias))
            {
                sql.Append("[t].[alias] = '");
                sql.Append(docTypeAlias);
                sql.Append("' and ");
            }
            sql.Append("[d].[published] = 1 and [n].[path] like '");
            if (ancestorId != -1)
            {
                sql.Append("%,");
            }
            sql.Append(ancestorId);
            sql.Append(",%'");

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(sql.ToString(), connection))
                {
                    command.CommandTimeout = ConnectionTimeOutInSeconds;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                            {
                                if (records.ContainsKey(reader.GetFieldValue<K>(0)))
                                {
                                    if (!ignoreDuplicates)
                                        throw new ArgumentException(string.Format("Can\'t add {0} value twice to {1}", reader.GetFieldValue<K>(0), keyPropertyAlias));
                                }
                                else
                                {
                                    records.Add(reader.GetFieldValue<K>(0), reader.IsDBNull(1) ? (V) NullField<V>() : reader.GetFieldValue<V>(1));
                                }
                            }
                        }
                    }
                }
            }
            return records;
        }

        public IDictionary<K, Tuple<V1, V2>> GetPublishedNodesOfProperties<K, V1, V2>(string connectionString, string docTypeAlias = null, int ancestorId = -1, bool ignoreDuplicates = false,
            string keyPropertyAlias = BuiltInPropertyAlias.Id,
            string value1PropertyAlias = BuiltInPropertyAlias.Name, 
            string value2PropertyAlias = BuiltInPropertyAlias.SortOrder)
        {
            var records = new Dictionary<K, Tuple<V1, V2>>();

            var sql = new StringBuilder();
            bool builtInPropertyAlias1 = BuiltInPropertyAlias.IsBuiltInPropertyAlias(keyPropertyAlias);
            bool builtInPropertyAlias2 = BuiltInPropertyAlias.IsBuiltInPropertyAlias(value1PropertyAlias);
            bool builtInPropertyAlias3 = BuiltInPropertyAlias.IsBuiltInPropertyAlias(value2PropertyAlias);

            sql.Append("select ");
            if (builtInPropertyAlias1)
                sql.Append(BuiltInPropertyAlias.Map(keyPropertyAlias));
            else
                sql.Append(MapPropertyDataField<K>(keyPropertyAlias, "pd1"));

            sql.Append(" as [key], ");
            if (builtInPropertyAlias2)
                sql.Append(BuiltInPropertyAlias.Map(value1PropertyAlias));
            else
                sql.Append(MapPropertyDataField<V1>(value1PropertyAlias, "pd2"));

            sql.Append(" as [value1], ");
            if (builtInPropertyAlias3)
                sql.Append(BuiltInPropertyAlias.Map(value2PropertyAlias));
            else
                sql.Append(MapPropertyDataField<V2>(value2PropertyAlias, "pd3"));
            sql.Append(@" as [value2] from [umbracoNode] as [n]
                        join [cmsDocument] as [d] on
		                    [d].[nodeId] = [n].[id]
	                    join [cmsContent] as [c] on
		                    [c].[nodeId] = [d].[nodeId]
	                    join [cmsContentType] as [t] on
		                    [t].[nodeId] = [c].[contentType]");

            if (!builtInPropertyAlias1)
            {
                sql.Append(SqlJoinPropertyData("pd1", keyPropertyAlias));
            }
            if (!builtInPropertyAlias2)
            {
                sql.Append(SqlJoinPropertyData("pd2", value1PropertyAlias));
            }
            if (!builtInPropertyAlias3)
            {
                sql.Append(SqlJoinPropertyData("pd3", value2PropertyAlias));
            }

            sql.Append(" where ");
            if (!string.IsNullOrWhiteSpace(docTypeAlias))
            {
                sql.Append("[t].[alias] = '");
                sql.Append(docTypeAlias);
                sql.Append("' and ");
            }
            sql.Append("[d].[published] = 1 and [n].[path] like '");
            if (ancestorId != -1)
            {
                sql.Append("%,");
            }
            sql.Append(ancestorId);
            sql.Append(",%'");

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(sql.ToString(), connection))
                {
                    command.CommandTimeout = ConnectionTimeOutInSeconds;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                            {
                                if (records.ContainsKey(reader.GetFieldValue<K>(0)))
                                {
                                    if (!ignoreDuplicates)
                                        throw new ArgumentException(string.Format("Can\'t add {0} value twice to {1}", reader.GetFieldValue<K>(0), keyPropertyAlias));
                                }
                                else
                                {
                                    records.Add(reader.GetFieldValue<K>(0),
                                        new Tuple<V1, V2>(reader.IsDBNull(1) ? (V1)NullField<V1>() : reader.GetFieldValue<V1>(1),
                                            reader.IsDBNull(2) ? (V2)NullField<V2>() : reader.GetFieldValue<V2>(2)));
                                }
                            }
                        }
                    }
                }
            }
            return records;
        }

        public IDictionary<K, Tuple<V1, V2, V3>> GetPublishedNodesOfProperties<K, V1, V2, V3>(string docTypeAlias = null, int ancestorId = -1, bool ignoreDuplicates = false,
            string keyPropertyAlias = BuiltInPropertyAlias.Id,
            string value1PropertyAlias = BuiltInPropertyAlias.Name,
            string value2PropertyAlias = BuiltInPropertyAlias.SortOrder,
            string value3PropertyAlias = BuiltInPropertyAlias.CreatedDate)
        {
            var records = new Dictionary<K, Tuple<V1, V2, V3>>();

            var sql = new StringBuilder();
            bool builtInPropertyAlias1 = BuiltInPropertyAlias.IsBuiltInPropertyAlias(keyPropertyAlias);
            bool builtInPropertyAlias2 = BuiltInPropertyAlias.IsBuiltInPropertyAlias(value1PropertyAlias);
            bool builtInPropertyAlias3 = BuiltInPropertyAlias.IsBuiltInPropertyAlias(value2PropertyAlias);
            bool builtInPropertyAlias4 = BuiltInPropertyAlias.IsBuiltInPropertyAlias(value3PropertyAlias);

            sql.Append("select ");
            if (builtInPropertyAlias1)
                sql.Append(BuiltInPropertyAlias.Map(keyPropertyAlias));
            else
                sql.Append(MapPropertyDataField<K>(keyPropertyAlias, "pd1"));

            sql.Append(" as [key], ");
            if (builtInPropertyAlias2)
                sql.Append(BuiltInPropertyAlias.Map(value1PropertyAlias));
            else
                sql.Append(MapPropertyDataField<V1>(value1PropertyAlias, "pd2"));

            sql.Append(" as [value1], ");
            if (builtInPropertyAlias3)
                sql.Append(BuiltInPropertyAlias.Map(value2PropertyAlias));
            else
                sql.Append(MapPropertyDataField<V2>(value2PropertyAlias, "pd3"));

            sql.Append(" as [value2], ");
            if (builtInPropertyAlias4)
                sql.Append(BuiltInPropertyAlias.Map(value3PropertyAlias));
            else
                sql.Append(MapPropertyDataField<V3>(value3PropertyAlias, "pd4"));

            sql.Append(@" as [value3] from [umbracoNode] as [n]
                        join [cmsDocument] as [d] on
		                    [d].[nodeId] = [n].[id]
	                    join [cmsContent] as [c] on
		                    [c].[nodeId] = [d].[nodeId]
	                    join [cmsContentType] as [t] on
		                    [t].[nodeId] = [c].[contentType]");

            if (!builtInPropertyAlias1)
            {
                sql.Append(SqlJoinPropertyData("pd1", keyPropertyAlias));
            }
            if (!builtInPropertyAlias2)
            {
                sql.Append(SqlJoinPropertyData("pd2", value1PropertyAlias));
            }
            if (!builtInPropertyAlias3)
            {
                sql.Append(SqlJoinPropertyData("pd3", value2PropertyAlias));
            }
            if (!builtInPropertyAlias4)
            {
                sql.Append(SqlJoinPropertyData("pd4", value2PropertyAlias));
            }

            sql.Append(" where ");
            if (!string.IsNullOrWhiteSpace(docTypeAlias))
            {
                sql.Append("[t].[alias] = '");
                sql.Append(docTypeAlias);
                sql.Append("' and ");
            }
            sql.Append("[d].[published] = 1 and [n].[path] like '");
            if (ancestorId != -1)
            {
                sql.Append("%,");
            }
            sql.Append(ancestorId);
            sql.Append(",%'");

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(sql.ToString(), connection))
                {
                    command.CommandTimeout = ConnectionTimeOutInSeconds;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                            {
                                if (records.ContainsKey(reader.GetFieldValue<K>(0)))
                                {
                                    if (!ignoreDuplicates)
                                        throw new ArgumentException(string.Format("Can\'t add {0} value twice to {1}", reader.GetFieldValue<K>(0), keyPropertyAlias));
                                }
                                else
                                {
                                    records.Add(reader.GetFieldValue<K>(0),
                                        new Tuple<V1, V2, V3>(reader.IsDBNull(1) ? (V1)NullField<V1>() : reader.GetFieldValue<V1>(1),
                                            reader.IsDBNull(2) ? (V2)NullField<V2>() : reader.GetFieldValue<V2>(2),
                                            reader.IsDBNull(3) ? (V3)NullField<V3>() : reader.GetFieldValue<V3>(3)));
                                }
                            }
                        }
                    }
                }
            }
            return records;
        }

        private bool IsNumeric(Type type)
        {
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;

                case TypeCode.Object:
                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        return IsNumeric(Nullable.GetUnderlyingType(type));
                    }
                    return false;

                default:
                    return false;
            }
        }

        private object BindSqlValueToPropertyType(SqlDataReader reader, int field, UmbracoPropertyAttribute.UmbracoPropertyType inputType, Type outputType)
        {
            object value = null;

            switch (inputType)
            {
                case UmbracoPropertyAttribute.UmbracoPropertyType.String:
                    {
                        string sqlValue = (reader.IsDBNull(field)) ? (IsNumeric(outputType) ? "0" : string.Empty) : reader.GetFieldValue<string>(field);
                        try
                        {
                            //  If your code falls over at the line below, its because you are mapping the wrong sql value to c# class property type
                            value = Convert.ChangeType(sqlValue, outputType, CultureInfo.InvariantCulture);
                        }
                        catch (Exception ex)
                        {
                            throw new FormatException("Unable to convert string value '" + sqlValue + "' to " + outputType.Name, ex);
                        }
                    }
                    break;

                case UmbracoPropertyAttribute.UmbracoPropertyType.Integer:
                    {
                        int sqlValue = reader.IsDBNull(field) ? 0 : reader.GetFieldValue<int>(field);
                        try
                        {
                            //  If your code falls over at the line below, its because you are mapping the wrong sql value to c# class property type
                            value = Convert.ChangeType(sqlValue, outputType, CultureInfo.InvariantCulture);
                        }
                        catch (Exception ex)
                        {
                            throw new FormatException("Unable to convert int value " + sqlValue.ToString() + " to " + outputType.Name, ex);
                        }
                    }
                    break;
                    
                case UmbracoPropertyAttribute.UmbracoPropertyType.DateTime:
                    {
                        DateTime sqlValue = reader.IsDBNull(field) ? DateTime.MinValue : reader.GetFieldValue<DateTime>(field);
                        try
                        {
                            //  If your code falls over at the line below, its because you are mapping the wrong sql value to c# class property type
                            value = Convert.ChangeType(sqlValue, outputType, CultureInfo.InvariantCulture);
                        }
                        catch (Exception ex)
                        {
                            throw new FormatException("Unable to convert DateTime value " + sqlValue.ToString() + " to " + outputType.Name, ex);
                        }
                    }
                    break;
            }
            return value;
        }

        public T GetPublishedNode<T>(int nodeId) where T : new()
        {
            var sql = new StringBuilder();
            var tables = new StringBuilder();
            var propertyTypes = new List<UmbracoPropertyAttribute.UmbracoPropertyType>();
            
            sql.Append("select ");

            int propertyCounter = 0;

            var properties = (typeof(T)).GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            if (properties.Count() == 0)
                throw new ArgumentException("Class " + typeof(T).Name + " has no public properties");

            foreach (var property in properties)
            {
                string sqlValue = "value" + propertyCounter.ToString();
                string sqlTable = "pd" + propertyCounter.ToString();
                string alias = char.ToLower(property.Name[0]) + property.Name.Substring(1);
                UmbracoPropertyAttribute.UmbracoPropertyType propertyType = UmbracoPropertyAttribute.UmbracoPropertyType.Auto;

                foreach (UmbracoPropertyAttribute attribute in property.GetCustomAttributes(typeof(UmbracoPropertyAttribute), true))
                {
                    if (!string.IsNullOrWhiteSpace(attribute.Alias))
                        alias = attribute.Alias;
                    propertyType = attribute.PropertyType;
                }

                if (propertyType == UmbracoPropertyAttribute.UmbracoPropertyType.Auto)
                {
                    if (property.PropertyType == typeof(int))
                        propertyType = UmbracoPropertyAttribute.UmbracoPropertyType.Integer;
                    else if (property.PropertyType == typeof(DateTime))
                        propertyType = UmbracoPropertyAttribute.UmbracoPropertyType.DateTime;
                    else
                        propertyType = UmbracoPropertyAttribute.UmbracoPropertyType.String;
                }

                propertyTypes.Add(propertyType);

                if (propertyCounter != 0)
                    sql.Append(',');

                if (BuiltInPropertyAlias.IsBuiltInPropertyAlias(alias))
                    sql.Append(BuiltInPropertyAlias.Map(alias));
                else
                {
                    switch (propertyType)
                    {
                        case UmbracoPropertyAttribute.UmbracoPropertyType.String:
                            sql.Append(MapPropertyDataField<string>(alias, sqlTable));
                            break;

                        case UmbracoPropertyAttribute.UmbracoPropertyType.Integer:
                            sql.Append(MapPropertyDataField<int>(alias, sqlTable));
                            break;

                        case UmbracoPropertyAttribute.UmbracoPropertyType.DateTime:
                            sql.Append(MapPropertyDataField<DateTime>(alias, sqlTable));
                            break;

                        case UmbracoPropertyAttribute.UmbracoPropertyType.NText:
                            sql.Append(sqlTable);
                            sql.Append(".[dataNtext]");
                            break;

                        case UmbracoPropertyAttribute.UmbracoPropertyType.NVarChar:
                            sql.Append(sqlTable);
                            sql.Append(".[dataNvarchar]");
                            break;
                    }

                    tables.Append(SqlJoinPropertyData(sqlTable, alias));

                }
                sql.Append(" as [");
                sql.Append(sqlValue);
                sql.Append(']');
                propertyCounter++;
            }

            sql.Append(" from [umbracoNode] as [n] join [cmsDocument] as [d] on [d].[nodeId] = [n].[id] join [cmsContent] as [c] on [c].[nodeId] = [d].[nodeId] join [cmsContentType] as [t] on [t].[nodeId] = [c].[contentType]");
            sql.Append(tables);

            sql.Append(" where [d].[published] = 1 and [n].[id] = ");
            sql.Append(nodeId);

            T result = new T();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(sql.ToString(), connection))
                {
                    command.CommandTimeout = ConnectionTimeOutInSeconds;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            propertyCounter = 0;
                            foreach (var property in properties)
                            {
                                object value = BindSqlValueToPropertyType(reader, propertyCounter, propertyTypes[propertyCounter], property.PropertyType);
                                property.SetValue(result, value);
                                propertyCounter++;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public IList<T> GetPublishedNodes<T>(string docTypeAlias = null, int ancestorId = -1) where T : new()
        {
            var sql = new StringBuilder();
            var tables = new StringBuilder();
            var propertyTypes = new List<UmbracoPropertyAttribute.UmbracoPropertyType>();

            sql.Append("select ");

            int propertyCounter = 0;

            var properties = (typeof(T)).GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            if (properties.Count() == 0)
                throw new ArgumentException("Class " + typeof(T).Name + " has no public properties");

            foreach (var property in properties)
            {
                string sqlValue = "value" + propertyCounter.ToString();
                string sqlTable = "pd" + propertyCounter.ToString();
                string alias = char.ToLower(property.Name[0]) + property.Name.Substring(1);
                UmbracoPropertyAttribute.UmbracoPropertyType propertyType = UmbracoPropertyAttribute.UmbracoPropertyType.Auto;

                foreach (UmbracoPropertyAttribute attribute in property.GetCustomAttributes(typeof(UmbracoPropertyAttribute), true))
                {
                    if (!string.IsNullOrWhiteSpace(attribute.Alias))
                        alias = attribute.Alias;
                    propertyType = attribute.PropertyType;
                }

                if (propertyType == UmbracoPropertyAttribute.UmbracoPropertyType.Auto)
                {
                    if (property.PropertyType == typeof(int))
                        propertyType = UmbracoPropertyAttribute.UmbracoPropertyType.Integer;
                    else if (property.PropertyType == typeof(DateTime))
                        propertyType = UmbracoPropertyAttribute.UmbracoPropertyType.DateTime;
                    else
                        propertyType = UmbracoPropertyAttribute.UmbracoPropertyType.String;
                }

                propertyTypes.Add(propertyType);

                if (propertyCounter != 0)
                    sql.Append(',');

                if (BuiltInPropertyAlias.IsBuiltInPropertyAlias(alias))
                    sql.Append(BuiltInPropertyAlias.Map(alias));
                else
                {
                    switch (propertyType)
                    {
                        case UmbracoPropertyAttribute.UmbracoPropertyType.String:
                            sql.Append(MapPropertyDataField<string>(alias, sqlTable));
                            break;

                        case UmbracoPropertyAttribute.UmbracoPropertyType.Integer:
                            sql.Append(MapPropertyDataField<int>(alias, sqlTable));
                            break;

                        case UmbracoPropertyAttribute.UmbracoPropertyType.DateTime:
                            sql.Append(MapPropertyDataField<DateTime>(alias, sqlTable));
                            break;

                        case UmbracoPropertyAttribute.UmbracoPropertyType.NText:
                            sql.Append(sqlTable);
                            sql.Append(".[dataNtext]");
                            break;

                        case UmbracoPropertyAttribute.UmbracoPropertyType.NVarChar:
                            sql.Append(sqlTable);
                            sql.Append(".[dataNvarchar]");
                            break;
                    }

                    tables.Append(SqlJoinPropertyData(sqlTable, alias));

                }
                sql.Append(" as [");
                sql.Append(sqlValue);
                sql.Append(']');
                propertyCounter++;
            }

            sql.Append(" from [umbracoNode] as [n] join [cmsDocument] as [d] on [d].[nodeId] = [n].[id] join [cmsContent] as [c] on [c].[nodeId] = [d].[nodeId] join [cmsContentType] as [t] on [t].[nodeId] = [c].[contentType]");
            sql.Append(tables);

            sql.Append(" where ");
            if (!string.IsNullOrWhiteSpace(docTypeAlias))
            {
                sql.Append("[t].[alias] = '");
                sql.Append(docTypeAlias);
                sql.Append("' and ");
            }
            sql.Append("[d].[published] = 1 and [n].[path] like '");
            if (ancestorId != -1)
            {
                sql.Append("%,");
            }
            sql.Append(ancestorId);
            sql.Append(",%'");

            List<T> results = new List<T>();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(sql.ToString(), connection))
                {
                    command.CommandTimeout = ConnectionTimeOutInSeconds;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            T result = new T();
                            propertyCounter = 0;
                            foreach (var property in properties)
                            {
                                object value = BindSqlValueToPropertyType(reader, propertyCounter, propertyTypes[propertyCounter], property.PropertyType);
                                property.SetValue(result, value);
                                propertyCounter++;
                            }
                            results.Add(result);
                        }
                    }
                }
            }
            return results;
        }

        public IList<T> GetPublishedNodes<T>(string[] docTypeAliases, int ancestorId = -1) where T : new()
        {
            var sql = new StringBuilder();
            var tables = new StringBuilder();
            var propertyTypes = new List<UmbracoPropertyAttribute.UmbracoPropertyType>();

            sql.Append("select ");

            int propertyCounter = 0;

            var properties = (typeof(T)).GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            if (properties.Count() == 0)
                throw new ArgumentException("Class " + typeof(T).Name + " has no public properties");

            foreach (var property in properties)
            {
                string sqlValue = "value" + propertyCounter.ToString();
                string sqlTable = "pd" + propertyCounter.ToString();
                string alias = char.ToLower(property.Name[0]) + property.Name.Substring(1);
                UmbracoPropertyAttribute.UmbracoPropertyType propertyType = UmbracoPropertyAttribute.UmbracoPropertyType.Auto;

                foreach (UmbracoPropertyAttribute attribute in property.GetCustomAttributes(typeof(UmbracoPropertyAttribute), true))
                {
                    if (!string.IsNullOrWhiteSpace(attribute.Alias))
                        alias = attribute.Alias;
                    propertyType = attribute.PropertyType;
                }

                if (propertyType == UmbracoPropertyAttribute.UmbracoPropertyType.Auto)
                {
                    if (property.PropertyType == typeof(int))
                        propertyType = UmbracoPropertyAttribute.UmbracoPropertyType.Integer;
                    else if (property.PropertyType == typeof(DateTime))
                        propertyType = UmbracoPropertyAttribute.UmbracoPropertyType.DateTime;
                    else
                        propertyType = UmbracoPropertyAttribute.UmbracoPropertyType.String;
                }

                propertyTypes.Add(propertyType);

                if (propertyCounter != 0)
                    sql.Append(',');

                if (BuiltInPropertyAlias.IsBuiltInPropertyAlias(alias))
                    sql.Append(BuiltInPropertyAlias.Map(alias));
                else
                {
                    switch (propertyType)
                    {
                        case UmbracoPropertyAttribute.UmbracoPropertyType.String:
                            sql.Append(MapPropertyDataField<string>(alias, sqlTable));
                            break;

                        case UmbracoPropertyAttribute.UmbracoPropertyType.Integer:
                            sql.Append(MapPropertyDataField<int>(alias, sqlTable));
                            break;

                        case UmbracoPropertyAttribute.UmbracoPropertyType.DateTime:
                            sql.Append(MapPropertyDataField<DateTime>(alias, sqlTable));
                            break;

                        case UmbracoPropertyAttribute.UmbracoPropertyType.NText:
                            sql.Append(sqlTable);
                            sql.Append(".[dataNtext]");
                            break;

                        case UmbracoPropertyAttribute.UmbracoPropertyType.NVarChar:
                            sql.Append(sqlTable);
                            sql.Append(".[dataNvarchar]");
                            break;
                    }

                    tables.Append(SqlJoinPropertyData(sqlTable, alias));

                }
                sql.Append(" as [");
                sql.Append(sqlValue);
                sql.Append(']');
                propertyCounter++;
            }

            sql.Append(" from [umbracoNode] as [n] join [cmsDocument] as [d] on [d].[nodeId] = [n].[id] join [cmsContent] as [c] on [c].[nodeId] = [d].[nodeId] join [cmsContentType] as [t] on [t].[nodeId] = [c].[contentType]");
            sql.Append(tables);

            sql.Append(" where [t].[alias] in (");
            bool docTypeAliasFirst = true;
            foreach (var docTypeAlias in docTypeAliases)
            {
                if (docTypeAliasFirst)
                    docTypeAliasFirst = false;
                else
                    sql.Append(',');
                sql.Append('\'');
                sql.Append(docTypeAlias);
                sql.Append('\'');
            }
            sql.Append(") and [d].[published] = 1 and [n].[path] like '");
            if (ancestorId != -1)
            {
                sql.Append("%,");
            }
            sql.Append(ancestorId);
            sql.Append(",%'");

            List<T> results = new List<T>();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(sql.ToString(), connection))
                {
                    command.CommandTimeout = ConnectionTimeOutInSeconds;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            T result = new T();
                            propertyCounter = 0;
                            foreach (var property in properties)
                            {
                                object value = BindSqlValueToPropertyType(reader, propertyCounter, propertyTypes[propertyCounter], property.PropertyType);
                                property.SetValue(result, value);
                                propertyCounter++;
                            }
                            results.Add(result);
                        }
                    }
                }
            }
            return results;
        }
    
        
    }
}
