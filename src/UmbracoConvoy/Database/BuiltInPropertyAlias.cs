﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UmbracoConvoy.Database
{
    static class BuiltInPropertyAlias
    {
        private const string hash = "#";

        public const string Id = hash + "id";
        public const string ParentId = hash + "parentid";
        public const string Name = hash + "name";
        public const string Level = hash + "level";
        public const string Path = hash + "path";
        public const string SortOrder = hash + "sortOrder";
        public const string CreatedDate = hash + "createDate";
        public const string DocTypeAlias = hash + "alias";
        public const string Icon = hash + "icon";
        public const string Thumbnail = hash + "thumbnail";
        public const string DocTypeDescription = hash + "description";

        internal static bool IsBuiltInPropertyAlias(string propertyAlias)
        {
            return (propertyAlias[0] == hash[0]) ? true : false;
        }

        internal static string Map(string propertyAlias)
        {
            switch (propertyAlias)
            {
                case BuiltInPropertyAlias.Id:
                    return "[n].[id]";

                case BuiltInPropertyAlias.ParentId:
                    return "[n].[parentID]";

                case BuiltInPropertyAlias.Name:
                    return "[n].[text]";

                case BuiltInPropertyAlias.Level:
                    return "[n].[level]";

                case BuiltInPropertyAlias.Path:
                    return "[n].[path]";

                case BuiltInPropertyAlias.SortOrder:
                    return "[n].[sortOrder]";

                case BuiltInPropertyAlias.CreatedDate:
                    return "[n].[createDate]";

                case BuiltInPropertyAlias.DocTypeAlias:
                    return "[t].[alias]";

                case BuiltInPropertyAlias.Icon:
                    return "[t].[icon]";

                case BuiltInPropertyAlias.Thumbnail:
                    return "[t].[thumbnail]";

                case BuiltInPropertyAlias.DocTypeDescription:
                    return "[t].[description]";

                default:
                    throw new ArgumentException("BuiltInPropertyAlias '" + propertyAlias + "' isn't a built-in property alias");
            }

        }

    }
}
