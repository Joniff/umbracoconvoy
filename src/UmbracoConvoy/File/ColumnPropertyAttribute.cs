﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UmbracoConvoy.File
{
    [AttributeUsage(AttributeTargets.All)]
    public class ColumnPropertyAttribute : System.Attribute
    {
        public string Name;
        public int? Index;  
        public object DefaultValue;
        public string DefaultFormat;
    
        public ColumnPropertyAttribute(string name, object defaultValue = null, string defaultFormat = null)
        {
            Name = name;
            DefaultValue = defaultValue;
            DefaultFormat = defaultFormat;
        }

        public ColumnPropertyAttribute(int index, object defaultValue = null, string defaultFormat = null)
        {
            Index = index;
            DefaultValue = defaultValue;
            DefaultFormat = defaultFormat;
        }
    }
}
