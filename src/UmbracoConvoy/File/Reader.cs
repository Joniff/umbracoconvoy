﻿using Excel;
using GenericParsing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UmbracoConvoy.File
{
    public class Reader
    {
        public delegate bool Processor<T>(string identifier, T record);
        public delegate bool ProcessorWithData<T, W>(string identifier, T record, W passedData);

        private class ColumnName
        {
            public int Index;
            public PropertyInfo Property;
            public object DefaultValue;
            public string DefaultFormat;
        }

        private Tuple<string, string> ToFilenameWordsheet(string filename)
        {
            var worksheetIndex = filename.IndexOf('#');
            if (worksheetIndex == -1)
            {
                return new Tuple<string, string>(filename, null);
            }

            if (worksheetIndex == 0 || worksheetIndex >= filename.Length - 1)
            {
                throw new ArgumentException("ERROR " + filename + " is not valid. Needs to be in format filename#worksheet");
            }

            return new Tuple<string,string>(filename.Substring(0, worksheetIndex - 1), filename.Substring(worksheetIndex + 1));
        }

        private int ReadExcelInternal<T, W>(string filename, bool firstRowIsHeader, bool useBinaryReader, object worksheet, object process, W passedObject) where T : new()
        {
            int processedRows = 0;
            using (FileStream stream = System.IO.File.Open(filename, FileMode.Open, FileAccess.Read))
            {
                IExcelDataReader excelReader = (useBinaryReader) ?
                    ExcelReaderFactory.CreateBinaryReader(stream) :
                    ExcelReaderFactory.CreateOpenXmlReader(stream);

                try
                {
                    excelReader.IsFirstRowAsColumnNames = firstRowIsHeader;
                    DataSet result = excelReader.AsDataSet();
                    if (result == null || !excelReader.IsValid)
                    {
                        throw new ArgumentException("ERROR " + filename + " is not a valid Excel spreadsheet");
                    }

                    int tableIndex = 0;
                    foreach (DataTable table in result.Tables)
                    {
                        if ((worksheet is int && tableIndex != (int) worksheet) ||
                            (worksheet is string && table.TableName != (string) worksheet))
                        {
                            continue;
                        }

                        int rowCount = 1;
                        List<ColumnName> header = null;
                        foreach (DataRow row in table.Rows)
                        {
                            if (rowCount == 1)
                            {
                                header = SetHeader<T>(row, firstRowIsHeader);
                                if (firstRowIsHeader)
                                {
                                    rowCount++;
                                    continue;
                                }
                            }
                            var identifier = string.Format("{0}#{1}", table.TableName, rowCount);
                            var data = MapRow<T>(header, row);
                            bool success = false;

                            if (process is Processor<T>)
                            {
                                success = ((Processor<T>) process).Invoke(identifier,  data);
                            }
                            else if (process is ProcessorWithData<T, W>)
                            {
                                success = ((ProcessorWithData<T, W>) process).Invoke(identifier,  data, passedObject);
                            }
                            if (success)
                            {
                                processedRows++;
                            }
                            rowCount++;
                        }
                    }
                }
                finally
                {
                    excelReader.Close();
                }
            }

            return processedRows;
        }

        public int ReadExcel<T>(string filename, bool firstRowIsHeader, bool useBinaryReader, Processor<T> process) where T : new()
        {
            var filenameWorksheet = ToFilenameWordsheet(filename);
            return ReadExcelInternal<T, Reader>(filenameWorksheet.Item1, firstRowIsHeader, useBinaryReader, filenameWorksheet.Item2, process, this);
        }

        public int ReadExcel<T, W>(string filename, bool firstRowIsHeader, bool useBinaryReader, ProcessorWithData<T, W> process, W passedObject) where T : new()
        {
            var filenameWorksheet = ToFilenameWordsheet(filename);
            return ReadExcelInternal<T, W>(filenameWorksheet.Item1, firstRowIsHeader, useBinaryReader, filenameWorksheet.Item2, process, passedObject);
        }

        public int ReadExcel<T>(string filename, bool firstRowIsHeader, bool useBinaryReader, int worksheetIndex, Processor<T> process) where T : new()
        {
            return ReadExcelInternal<T, Reader>(ToFilenameWordsheet(filename).Item1, firstRowIsHeader, useBinaryReader, worksheetIndex, process, this);
        }

        public int ReadExcel<T, W>(string filename, bool firstRowIsHeader, bool useBinaryReader, int worksheetIndex, ProcessorWithData<T, W> process, W passedObject) where T : new()
        {
            return ReadExcelInternal<T, W>(ToFilenameWordsheet(filename).Item1, firstRowIsHeader, useBinaryReader, worksheetIndex, process, passedObject);
        }

        public int ReadExcel<T>(string filename, bool firstRowIsHeader, bool useBinaryReader, string worksheetName, Processor<T> process) where T : new()
        {
            return ReadExcelInternal<T, Reader>(ToFilenameWordsheet(filename).Item1, firstRowIsHeader, useBinaryReader, worksheetName, process, this);
        }

        public int ReadExcel<T, W>(string filename, bool firstRowIsHeader, bool useBinaryReader, string worksheetName, ProcessorWithData<T, W> process, W passedObject) where T : new()
        {
            return ReadExcelInternal<T, W>(ToFilenameWordsheet(filename).Item1, firstRowIsHeader, useBinaryReader, worksheetName, process, passedObject);
        }

        private int ReadTextInternal<T, W>(string filename, bool firstRowIsHeader, object columnDelimiterOrConfigFile, object process, W passedObject) where T : new()
        {
            int processedRows = 0;
            using (GenericParserAdapter parser = new GenericParserAdapter(filename))
            {
                if (columnDelimiterOrConfigFile is string)
                {
                    parser.Load((string) columnDelimiterOrConfigFile);
                }
                else if (columnDelimiterOrConfigFile is char)
                {
                    parser.ColumnDelimiter = (char) columnDelimiterOrConfigFile;
                }
                else
                {
                    parser.ColumnDelimiter = '\t';
                }

                DataTable table = parser.GetDataTable();
                if (table == null)
                {
                    throw new ArgumentException("ERROR " + filename + " is not a valid text file");
                }

                int rowCount = 1;
                List<ColumnName> header = null;
                foreach (DataRow row in table.Rows)
                {
                    if (rowCount == 1)
                    {
                        header = SetHeader<T>(row, firstRowIsHeader);
                        if (firstRowIsHeader)
                        {
                            rowCount++;
                            continue;
                        }
                    }
                            
                    var identifier = rowCount.ToString();
                    var data = MapRow<T>(header, row);
                    bool success = false;

                    if (process is Processor<T>)
                    {
                        success = ((Processor<T>) process).Invoke(identifier,  data);
                    }
                    else if (process is ProcessorWithData<T, W>)
                    {
                        success = ((ProcessorWithData<T, W>) process).Invoke(identifier,  data, passedObject);
                    }
                    if (success)
                    {
                        processedRows++;
                    }
                    rowCount++;
                }
            }

            return processedRows;
        }

        private string TextConfigFile(string extension)
        {
            var configFile = Path.GetDirectoryName((new UriBuilder(Assembly.GetExecutingAssembly().CodeBase)).Path) + "\\" + extension + ".xml";
            if (System.IO.File.Exists(configFile))
            {
                return configFile;
            }
            return null;
        }

        public int ReadText<T>(string filename, bool firstRowIsHeader, string configFilename, Processor<T> process) where T : new()
        {
            return ReadTextInternal<T, Reader>(ToFilenameWordsheet(filename).Item1, firstRowIsHeader, configFilename, process, this);
        }

        public int ReadText<T, W>(string filename, bool firstRowIsHeader, string configFilename, ProcessorWithData<T, W> process, W passedObject) where T : new()
        {
            return ReadTextInternal<T, W>(ToFilenameWordsheet(filename).Item1, firstRowIsHeader, configFilename, process, passedObject);
        }

        public int ReadText<T>(string filename, bool firstRowIsHeader, char columnDelimiter, Processor<T> process) where T : new()
        {
            return ReadTextInternal<T, Reader>(ToFilenameWordsheet(filename).Item1, firstRowIsHeader, columnDelimiter, process, this);
        }

        public int ReadText<T, W>(string filename, bool firstRowIsHeader, char columnDelimiter, ProcessorWithData<T, W> process, W passedObject) where T : new()
        {
            return ReadTextInternal<T, W>(ToFilenameWordsheet(filename).Item1, firstRowIsHeader, columnDelimiter, process, passedObject);
        }

        private int ReadInternal<T, W>(string filename, bool firstRowIsHeader, object process, W passedObject) where T : new()
        {
            char columnDelimiter = '\t';
            var filenameWorksheet = ToFilenameWordsheet(filename);
            var extension = Path.GetExtension(filenameWorksheet.Item1).Substring(1).ToLowerInvariant();
            int worksheetIndex;

            switch (extension)
            {
                case "xls":			//	Excel 97-2003
                    return ReadExcelInternal<T, W>(filename, firstRowIsHeader, true, 
                        int.TryParse(filenameWorksheet.Item2, out worksheetIndex) ? (object) worksheetIndex : (object) filenameWorksheet.Item2, process, passedObject);
                    /* Want to fall into next case */

                case "xlsx":			//	Excel 2007
                case "ods":			//	Open document spreadsheet
                    return ReadExcelInternal<T, W>(filename, firstRowIsHeader, false, 
                        int.TryParse(filenameWorksheet.Item2, out worksheetIndex) ? (object) worksheetIndex : (object) filenameWorksheet.Item2, process, passedObject);

                case "csv":			//	Comma seperated values
                    columnDelimiter = ',';
                    break;

                case "psv":			//	Pipe seperated values
                    columnDelimiter = '|';
                    break;
            }
            var configFile = TextConfigFile(extension);
            if (configFile != null)
            {
                return ReadTextInternal<T, W>(filenameWorksheet.Item1, firstRowIsHeader, configFile, process, passedObject);
            }
            return ReadTextInternal<T, W>(filenameWorksheet.Item1, firstRowIsHeader, columnDelimiter, process, passedObject);
        }


        /// <summary>
        /// Read a text or Excel file
        /// </summary>
        /// <typeparam name="T">Class used to repersent each data record</typeparam>
        /// <param name="filename">Filename or filename#worksheet if an Excel file</param>
        /// <param name="firstRowIsHeader">Does the first row contain column headerings</param>
        /// <param name="process">Method called for every data record found</param>
        /// <returns>Number of data records found</returns>
        public int Read<T>(string filename, bool firstRowIsHeader, Processor<T> process) where T : new()
        {
            return ReadInternal<T, Reader>(filename, firstRowIsHeader, process, this);
        }

        /// <summary>
        /// Read a text or Excel file
        /// </summary>
        /// <typeparam name="T">Class used to repersent each data record</typeparam>
        /// <param name="filename">Filename or filename#worksheet if an Excel file</param>
        /// <param name="firstRowIsHeader">Does the first row contain column headerings</param>
        /// <param name="process">Method called for every data record found</param>
        /// <returns>Number of data records found</returns>
        public int Read<T, W>(string filename, bool firstRowIsHeader, ProcessorWithData<T, W> process, W passedObject) where T : new()
        {
            return ReadInternal<T, W>(filename, firstRowIsHeader, process, passedObject);
        }

        private List<ColumnName> SetHeader<T>(DataRow row, bool useNames = true)
        {
            var header = new List<ColumnName>();
            var properties = (typeof(T)).GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            if (properties.Count() == 0)
                throw new ArgumentException("Class " + typeof(T).Name + " has no public properties");

            int propertyIndex = 0;
            foreach (var property in properties)
            {
                int? columnIndex = null;
                string columnName = property.Name;
                object columnDefaultValue = null;
                string columnDefaultFormat = null;

                foreach (ColumnPropertyAttribute attribute in property.GetCustomAttributes(typeof(ColumnPropertyAttribute), true))
                {
                    if (attribute.Index != null)
                    {
                        columnIndex = (int) attribute.Index;
                    }
                    if (useNames && attribute.Name != null)
                    {
                        columnName = (string) attribute.Name;
                    }
                    if (attribute.DefaultValue != null)
                    {
                        columnDefaultValue = attribute.DefaultValue;
                    }
                    if (attribute.DefaultFormat != null)
                    {
                        columnDefaultFormat = attribute.DefaultFormat;
                    }
                }
                foreach (System.ComponentModel.DefaultValueAttribute attribute in property.GetCustomAttributes(typeof(System.ComponentModel.DefaultValueAttribute), true))
                {
                    if (attribute.Value != null)
                    {
                        columnDefaultValue = attribute.Value;
                    }
                }

                if (useNames && columnIndex == null)
                {
                    int itemArrayIndex = 0;
                    foreach (var item in row.ItemArray)
                    {
                        if (item is string && string.Compare((string) item, columnName, true) == 0)
                        {
                            columnIndex = itemArrayIndex;
                            break;
                        }
                        itemArrayIndex++;
                    }
                }
                if (columnIndex == null)
                {
                    columnIndex = propertyIndex;
                }

                header.Add(new ColumnName() {
                    Index = (int) columnIndex, 
                    Property = property, 
                    DefaultValue = columnDefaultValue,
                    DefaultFormat = columnDefaultFormat
                });
                propertyIndex = ((int) columnIndex) + 1;
            }
            return header;
        }

        private T MapRow<T>(List<ColumnName> header, DataRow row) where T : new()
        {
            T result = new T();

            foreach (var column in header)
            {
                object value = null;
                if (column.Index < row.ItemArray.Length && !row.IsNull(column.Index))
                {
                    //  See if we can load value for column
                    if (column.Property.PropertyType == typeof(DateTime) && column.DefaultFormat != null)
                    {
                        DateTime date;
                        if (DateTime.TryParseExact(row.Field<string>(column.Index), column.DefaultFormat, System.Globalization.CultureInfo.InvariantCulture, 
                            System.Globalization.DateTimeStyles.AssumeLocal, out date))
                        {
                            value = date;
                        }
                        else if (DateTime.TryParse(row.Field<string>(column.Index), System.Globalization.CultureInfo.InvariantCulture, 
                            System.Globalization.DateTimeStyles.AssumeLocal, out date))
                        {
                            value = date;
                        }
                    }
                    else if (column.Property.PropertyType == typeof(bool))
                    {
                        string strValue = (string) Convert.ChangeType(row.ItemArray[column.Index], typeof(string), CultureInfo.InvariantCulture);
                        if (!string.IsNullOrWhiteSpace(strValue))
                        {
                            switch (strValue[0])
                            {
                                case '0':
                                case 'f':
                                case 'F':
                                case 'n':
                                case 'N':
                                    value = false;
                                    break;
  
                                default:
                                    value = true;
                                    break;
                            }
                        }
                    }

                    if (value == null)
                    {
                        try
                        {
                            value = Convert.ChangeType(row.ItemArray[column.Index], column.Property.PropertyType, CultureInfo.InvariantCulture);
                        }
                        catch (Exception)
                        {
                            //  Swallow exception
                        }
                    }
                }
                if (value == null)
                {
                    //  Use default value
                    if (column.DefaultValue != null)
                    {
                        try
                        {
                            value = Convert.ChangeType(column.DefaultValue, column.Property.PropertyType, CultureInfo.InvariantCulture);
                        }
                        catch (Exception)
                        {
                            //  Swallow exception
                        }
                    }
                    if (value == null)
                    {
                        if (column.Property.PropertyType == typeof(string))
                            value = "";
                        else
                            value = Activator.CreateInstance(column.Property.PropertyType);
                    }
                }
                column.Property.SetValue(result, value);
            }

            return result;
        }
    }
}
