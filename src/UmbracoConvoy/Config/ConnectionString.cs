﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UmbracoConvoy.Config
{
    public static class ConnectionString
    {
        private const string UmbracoDbDSN = "umbracoDbDSN";     //  Umbraco.Core.Configuration.GlobalSettings.UmbracoConnectionName

        public static string Umbraco
        {
            get
            {
                using (var exeConfiguration = new Config.Models.ExeConfiguration())
                {
                    return exeConfiguration.GetConnectionString(UmbracoDbDSN);
                }
            }
            set
            {
                using (var exeConfiguration = new Config.Models.ExeConfiguration())
                {
                    exeConfiguration.SetConnectionString(UmbracoDbDSN, value);
                }
            }
        }
    }
}
