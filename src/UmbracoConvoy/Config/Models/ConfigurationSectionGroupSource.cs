﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;

namespace UmbracoConvoy.Config.Models
{
    public class ConfigurationSectionGroupSource : ConfigurationSectionGroup
    {
        public ConfigurationSectionGroupSource() 
        { 
        }

        protected override bool ShouldSerializeSectionGroupInTargetVersion(FrameworkName targetFramework)
        {
            return false;
        }
    }
}
