﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UmbracoConvoy.Config.Models
{
    public class ConfigurationSectionSource : ConfigurationSection
    {
        public ConfigurationSectionSource() 
        { 
        }

        public ConfigurationSectionSource(string type, string source, bool? requirePermission = null) 
        { 
            SectionInformation.Type = type;
            if (requirePermission != null)
            {
                SectionInformation.RequirePermission = (bool) requirePermission;
            }
            if (!string.IsNullOrWhiteSpace(source))
            {
                SectionInformation.ConfigSource = source;
            }
        }

    }
}
