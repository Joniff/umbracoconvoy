﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace UmbracoConvoy.Config.Models
{
    public class ExeConfiguration : IDisposable
    {
        private Configuration configuration = null;
        private bool dirty = false;
        private List<Tuple<string, string>> replaces = new List<Tuple<string, string>>();
        private List<string> refreshes = new List<string>();
        private List<string> deleteFiles = new List<string>();
        private bool refreshConfig;

        public ExeConfiguration(Configuration overrideConfiguration, bool refresh = false)
        {
            configuration = overrideConfiguration;
            refreshConfig = refresh;
        }

        public ExeConfiguration(bool refresh = false)
        {
            configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (configuration == null)
            {
                throw new ConfigurationErrorsException(string.Format("Unable to find a config file in {0}", Assembly.GetExecutingAssembly().Location));
            }
            refreshConfig = refresh;
        }

        public void Dispose()
        {
            if (configuration != null && dirty)
                Save();

            configuration = null;
        }
        public string FilePath
        {
            get
            {
                return configuration.FilePath;
            }
        }

        public ConnectionStringsSection ConnectionStrings
        {
            get
            {
                return configuration.ConnectionStrings;
            }
        }

        public void Save()
        {
            foreach (ConfigurationSection section in configuration.Sections)
            {
                try
                {
                    System.Configuration.ConfigurationManager.RefreshSection(section.SectionInformation.SectionName);
                }
                catch (Exception)
                {
                }
            }
            foreach (ConfigurationSectionGroup group in configuration.SectionGroups)
            {
                foreach (ConfigurationSection section in group.Sections)
                {
                    try
                    {
                        System.Configuration.ConfigurationManager.RefreshSection(section.SectionInformation.SectionName);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            try
            {
                configuration.Save(ConfigurationSaveMode.Minimal, false);
            }
            catch (ConfigurationErrorsException ex)
            {
            }
            
            if (replaces.Any())
            {
                string fileContents = System.IO.File.ReadAllText(configuration.FilePath);
                foreach (var replace in replaces)
                {
                    fileContents = fileContents.Replace(replace.Item1, replace.Item2);
                }
                System.IO.File.WriteAllText(configuration.FilePath, fileContents);
                replaces.Clear();
            }
        
            foreach (var refresh in refreshes)
            {
                System.Configuration.ConfigurationManager.RefreshSection(refresh);
            }
            refreshes.Clear();

            foreach (var deleteFile in deleteFiles)
            {
                System.IO.File.Delete(deleteFile);
            }
            deleteFiles.Clear();

            dirty = false;

        }

        public string FormatContent(string name, string contents)
        {
            var xml = new XmlDocument();
            xml.LoadXml(contents);
            StringBuilder strippedContent = new StringBuilder();
            strippedContent.Append("<");
            strippedContent.Append(name);
            strippedContent.Append(">");

            foreach (XmlNode node in xml.ChildNodes)
            {
                if (node.NodeType != XmlNodeType.XmlDeclaration && node.NodeType != XmlNodeType.Comment)
                     strippedContent.AppendLine(node.InnerXml);
            }
            strippedContent.Append("</");
            strippedContent.Append(name);
            strippedContent.Append(">");
            strippedContent.Replace("><",">\n<");
            return strippedContent.ToString();
        }

        public void CreateSection(string name, string type, bool? requirePermission, string file, string contents)
        {
            if (configuration.Sections[name] != null)
            {
                if (!refreshConfig)
                {
                    return;
                }
                configuration.Sections.Remove(name);
            }
            var marker = Guid.NewGuid().ToString() + ".tmp";
            deleteFiles.Add(Path.Combine(Path.GetDirectoryName(configuration.FilePath), marker));
            configuration.Sections.Add(name, new ConfigurationSectionSource(type, marker, requirePermission));

            replaces.Add(new Tuple<string,string>(string.Format("<{0} configSource=\"{1}\" />", name, marker), FormatContent(name, contents)));

            refreshes.Add(name);
            dirty = true;
        }

        public void CreateSectionGroup(string group, string name, string type, bool? requirePermission, string file, string contents)
        {
            if (configuration.SectionGroups[group] == null)
            {
                configuration.SectionGroups.Add(group, new ConfigurationSectionGroupSource() );
            }
            var sectionGroup = configuration.SectionGroups[group];

            if (sectionGroup.Sections[name] != null)
            {
                if (!refreshConfig)
                {
                    return;
                }
                sectionGroup.Sections.Remove(name);
            }

            var marker = Guid.NewGuid().ToString() + ".tmp";
            deleteFiles.Add(Path.Combine(Path.GetDirectoryName(configuration.FilePath), marker));
            sectionGroup.Sections.Add(name, new ConfigurationSectionSource(type, marker, requirePermission));

            replaces.Add(new Tuple<string,string>(string.Format("<{0} configSource=\"{1}\" />", name, marker), FormatContent(name, contents)));

            var toRemove = string.Format(" type=\"{0}\" ", sectionGroup.Type);
            if (!replaces.Any(x => x.Item1 == toRemove))
            {
                replaces.Add(new Tuple<string, string>(toRemove, ""));
            }
            refreshes.Add(group + "/" + name);
            dirty = true;
        }

        public void RemoveSectionGroup(string group)
        {
            if (!refreshConfig)
            {
                return;
            }
            configuration.SectionGroups.Remove(group);
            refreshes.Add(group);
            dirty = true;
        }

        public string GetConnectionString(string name)
        {
            if (configuration.ConnectionStrings == null)
            {
                throw new ConfigurationErrorsException(string.Format("File {0}, Section called ConnectionStrings doesn\'t exist", configuration.FilePath));
            }

            var connectionNode = configuration.ConnectionStrings.ConnectionStrings[name];
            if (connectionNode == null)
            {
                throw new ConfigurationErrorsException(string.Format("File {0}, Section ConnectionStrings doesn\'t contain a connection string called {1}", 
                    configuration.FilePath, name));
            }

            var connectionString = connectionNode.ConnectionString;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ConfigurationErrorsException(string.Format("File {0}, Section ConnectionStrings, Connection string {1} doesn't have a connectionString attribute", 
                    configuration.FilePath, name));
            }
            return connectionString;
        }

        public void SetConnectionString(string name, string value)
        {
            if (configuration.ConnectionStrings != null && configuration.ConnectionStrings.ConnectionStrings[name] != null)
            {
                if (!refreshConfig && configuration.ConnectionStrings.ConnectionStrings[name].ConnectionString == value)
                {
                    return;
                }

                configuration.ConnectionStrings.ConnectionStrings.Remove(configuration.ConnectionStrings.ConnectionStrings[name]);
            }
            if (!string.IsNullOrWhiteSpace(value))
            {
                configuration.ConnectionStrings.ConnectionStrings.Add(new ConnectionStringSettings(name, value));
            }
            refreshes.Add("connectionStrings");
            dirty = true;
        }

    }
}
