﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace UmbracoConvoy.ExtensionMethods
{
    public static class IMediaExtenstion
    {
        public static bool Attach(this IMedia media, string filename)
        {
            bool isLocal = (filename.StartsWith("http://", true, System.Globalization.CultureInfo.InvariantCulture) || 
                filename.StartsWith("https://", true, System.Globalization.CultureInfo.InvariantCulture) ||
                    filename.StartsWith("ftp://", true, System.Globalization.CultureInfo.InvariantCulture)) ? false : new Uri(filename).IsFile;

            if (!isLocal)
            {
                var request = WebRequest.Create(filename);
                using (var response = request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        if (stream == null)
                        {
                            return false;
                        }

                        using (var ms = new MemoryStream())
                        {
                            stream.CopyTo(ms);
                            media.SetValue(Umbraco.Core.Constants.Conventions.Media.File, Path.GetFileName(filename), ms);
                            //Services.MediaService.Save(media);
                            return true;
                        }
                    }
                }
            }

            using (var ms = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                media.SetValue(Umbraco.Core.Constants.Conventions.Media.File, Path.GetFileName(filename), ms);
                //Services.MediaService.Save(media);
            }
            return true;
        }

    }
}
