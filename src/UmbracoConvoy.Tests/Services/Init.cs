﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UmbracoConvoy.Tests
{
    [TestClass]
    public class Init
    {
        [TestMethod]
        public void Test()
        {
            var umbracoSettings = System.Configuration.ConfigurationManager.GetSection("umbracoConfiguration/settings");
            var baseRestExtensions = System.Configuration.ConfigurationManager.GetSection("umbracoConfiguration/BaseRestExtensions");              
            var dashboardConfig = System.Configuration.ConfigurationManager.GetSection("umbracoConfiguration/dashBoard");                

            //Assert.IsNotNull(umbracoSettings);
            //Assert.IsNotNull(baseRestExtensions);
            //Assert.IsNotNull(dashboardConfig);
        }

        [TestMethod]
        public void CreateViaConnectionString()
        {
            var access = new UmbracoConvoy.Services.Access("server=(local);database=UmbracoConvoy;user id=umbraco;password=umbraco");

            Assert.IsNotNull(access);

            //Assert.IsTrue(access.UpgradeSchemaAndData());

        }

    }
}
