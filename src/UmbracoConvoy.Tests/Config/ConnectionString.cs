﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;

namespace UmbracoConvoy.Tests.Config
{
    [TestClass]
    public class ConnectionString
    {
        [TestMethod]
        public void GetAndSet()
        {
            string[] connectionStrings = 
            {
                "TestConnectionString",
                "Data Source=.;Initial Catalog=test1;Persist Security Info=True;User ID=umbraco;Password=umbraco",
                "Server=localhost;Database=test2;user id=umbraco;Password=umbraco;",
                "server=(local);database=UmbracoConvoy;user id=umbraco;password=umbraco"
            };

            foreach (var cs in connectionStrings)
            {
                UmbracoConvoy.Config.ConnectionString.Umbraco = cs;
                Assert.AreEqual<string>(UmbracoConvoy.Config.ConnectionString.Umbraco, cs);

                System.Configuration.ConfigurationManager.RefreshSection("connectionStrings");
                Assert.AreEqual<string>(System.Configuration.ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString, cs);
            }
        }

        [TestMethod]
        public void Clear()
        {
            UmbracoConvoy.Config.ConnectionString.Umbraco = null;
            try
            {
                var cs = UmbracoConvoy.Config.ConnectionString.Umbraco;
            }
            catch (ConfigurationErrorsException)
            {
                //  We expect this exception as the connection string has been removed
                return;
            }
            Assert.Fail();
        }
    }
}
