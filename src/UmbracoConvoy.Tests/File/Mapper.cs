﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UmbracoConvoy.File;

namespace UmbracoConvoy.Tests.File
{
    [TestClass]
    public class Mapper
    {
        public class TestRecord
        {
            public string Indentifier;

            [ColumnProperty("text")]

            public string Text { get; set; }

            [ColumnProperty("number")]
            public int Number { get; set; }

            [ColumnProperty("currency")]
            public decimal Money { get; set; }

            [ColumnProperty("ddmmyyyy", 0L, "ddMMyyyy")]
            public DateTime Date1 { get; set; }

            [ColumnProperty("yyyymmmdd", 0L, "yyyyMMMdd")]
            public DateTime Date2 { get; set; }
        }

        readonly static TestRecord[] records = new TestRecord[]
        {
            new TestRecord(){ Indentifier = "2", Text = "hello", Number = 1, Money = 23.45m, Date1 = new DateTime(2015, 01, 01), Date2 = new DateTime(2014, 02, 02) },
            new TestRecord(){ Indentifier = "3", Text = "goodbye", Number = -1, Money = -11.22m, Date1 = new DateTime(1985, 12, 25), Date2 = new DateTime(2016, 02, 02) },
            new TestRecord(){ Indentifier = "4", Text = "a'b'c'd'ef", Number = 9999, Money = 99.01m, Date1 = new DateTime(2041, 08, 14), Date2 = new DateTime(2017, 9, 28) },
            new TestRecord(){ Indentifier = "5", Text = "super duper", Number = -9999, Money = -999.9m, Date1 = new DateTime(2004, 02, 29), Date2 = new DateTime(2029, 12, 31) },
            new TestRecord(){ Indentifier = "6", Text = "Speeder go go hello", Number = 0, Money = 917m, Date1 = DateTime.MinValue, Date2 = DateTime.MinValue },
            new TestRecord(){ Indentifier = "7", Text = "hello, world", Number = 0, Money = 917m, Date1 = DateTime.MinValue, Date2 = DateTime.MinValue }
        };

        public static bool Process<@TestRecord>(string identifier, Mapper.TestRecord record)
        {
            foreach (var rec in records)
            {
                if (rec.Indentifier == identifier)
                {
                    Assert.AreEqual(rec.Text, record.Text);
                    Assert.AreEqual(rec.Number, record.Number);
                    Assert.AreEqual(rec.Money, record.Money);
                    Assert.AreEqual(rec.Date1, record.Date1);
                    Assert.AreEqual(rec.Date2, record.Date2);
                    return true;
                }
            }
            Assert.Fail();
            return false;
        }
        
        public static bool ProcessWithData<@TestRecord, @string>(string identifier, Mapper.TestRecord record, string culture)
        {
            Assert.AreEqual(culture, Thread.CurrentThread.CurrentCulture.IetfLanguageTag);
            
            foreach (var rec in records)
            {
                if (rec.Indentifier == identifier)
                {
                    Assert.AreEqual(rec.Text, record.Text);
                    Assert.AreEqual(rec.Number, record.Number);
                    Assert.AreEqual(rec.Money, record.Money);
                    Assert.AreEqual(rec.Date1, record.Date1);
                    Assert.AreEqual(rec.Date2, record.Date2);
                    return true;
                }
            }
            Assert.Fail();
            return false;
        }

        [TestMethod]

        public void Convertors()
        {
            List<Tuple<string, char, string>> formats = new List<Tuple<string,char, string>>()
            {
                new Tuple<string, char, string>("csv", ',', "ar-EG"),
                new Tuple<string, char, string>("psv", '|', "ar-SA"),
                new Tuple<string, char, string>("tab", '\t', "zh-CN"),

                new Tuple<string, char, string>("csv", ',', "en-US"),
                new Tuple<string, char, string>("psv", '|', "en-GB"),
                new Tuple<string, char, string>("tab", '\t', ""),

                new Tuple<string, char, string>("csv", ',', "hr-HR"),
                new Tuple<string, char, string>("psv", '|', "cs-CZ"),
                new Tuple<string, char, string>("tab", '\t', "da-DK"),

                new Tuple<string, char, string>("csv", ',', "nl-NL"),
                new Tuple<string, char, string>("psv", '|', "fa-IR"),
                new Tuple<string, char, string>("tab", '\t', "fi-FI"),

                new Tuple<string, char, string>("csv", ',', "fr-FR"),
                new Tuple<string, char, string>("psv", '|', "de-DE"),
                new Tuple<string, char, string>("tab", '\t', "el-GR"),

                new Tuple<string, char, string>("csv", ',', "gu-IN"),
                new Tuple<string, char, string>("psv", '|', "hi-IN"),
                new Tuple<string, char, string>("tab", '\t', "hu-HU"),
            
                new Tuple<string, char, string>("csv", ',', "it-IT"),
                new Tuple<string, char, string>("psv", '|', "ja-JP"),
                new Tuple<string, char, string>("tab", '\t', "ko-KR"),
            
                new Tuple<string, char, string>("csv", ',', "pl-PL"),
                new Tuple<string, char, string>("psv", '|', "es-ES"),
                new Tuple<string, char, string>("tab", '\t', "tr-TR"),
            
                new Tuple<string, char, string>("csv", ',', ""),
                new Tuple<string, char, string>("psv", '|', ""),
                new Tuple<string, char, string>("tab", '\t', "vi-VN")
            
            };

            foreach (var format in formats)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(format.Item3);
                
                //  Forward
                var filename = Path.GetTempFileName() + "." + format.Item1;
                try
                {
                    using (StreamWriter writer = new StreamWriter(filename))
                    {
                        writer.Write("text");
                        writer.Write(format.Item2);
                        writer.Write("number");
                        writer.Write(format.Item2);
                        writer.Write("currency");
                        writer.Write(format.Item2);
                        writer.Write("ddmmyyyy");
                        writer.Write(format.Item2);
                        writer.WriteLine("yyyymmmdd");

                        foreach (var rec in records)
                        {
                            if (rec.Text.IndexOf(format.Item2) != -1)
                                writer.Write('"');
                            writer.Write(rec.Text);
                            if (rec.Text.IndexOf(format.Item2) != -1)
                                writer.Write('"');
                            writer.Write(format.Item2);
                            writer.Write(rec.Number);
                            writer.Write(format.Item2);
                            writer.Write(rec.Money.ToString("#0.00", System.Globalization.CultureInfo.InvariantCulture));
                            writer.Write(format.Item2);
                            if (rec.Date1 != DateTime.MinValue)
                                writer.Write(rec.Date1.ToString("ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture));
                            writer.Write(format.Item2);
                            if (rec.Date2 != DateTime.MinValue)
                                writer.Write(rec.Date2.ToString("yyyyMMMdd", System.Globalization.CultureInfo.InvariantCulture));
                            writer.WriteLine("");
                        }
                    }
                    var reader = new UmbracoConvoy.File.Reader();
                    int recordsRead = reader.Read<Mapper.TestRecord>(filename, true, Mapper.Process<TestRecord>);
                    Assert.AreEqual(recordsRead, records.Length);
                    recordsRead = reader.Read<Mapper.TestRecord, string>(filename, true, Mapper.ProcessWithData<TestRecord, Tuple<string, char, string>>, format.Item3);
                    Assert.AreEqual(recordsRead, records.Length);
                }
                finally
                {
                    System.IO.File.Delete(filename);
                }

                //  Backwards
                var filename2 = Path.GetTempFileName() + "." + format.Item1;
                try
                {
                    using (StreamWriter writer = new StreamWriter(filename2))
                    {
                        writer.Write("yyyymmmdd");
                        writer.Write(format.Item2);
                        writer.Write("ddmmyyyy");
                        writer.Write(format.Item2);
                        writer.Write("currency");
                        writer.Write(format.Item2);
                        writer.Write("number");
                        writer.Write(format.Item2);
                        writer.WriteLine("text");

                        foreach (var rec in records)
                        {
                            if (rec.Date2 != DateTime.MinValue)
                                writer.Write(rec.Date2.ToString("yyyyMMMdd", System.Globalization.CultureInfo.InvariantCulture));
                            writer.Write(format.Item2);
                            if (rec.Date1 != DateTime.MinValue)
                                writer.Write(rec.Date1.ToString("ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture));
                            writer.Write(format.Item2);
                            writer.Write(rec.Money.ToString("#0.00", System.Globalization.CultureInfo.InvariantCulture));
                            writer.Write(format.Item2);
                            writer.Write(rec.Number);
                            writer.Write(format.Item2);
                            if (rec.Text.IndexOf(format.Item2) != -1)
                                writer.Write('"');
                            writer.Write(rec.Text);
                            if (rec.Text.IndexOf(format.Item2) != -1)
                                writer.Write('"');
                            writer.WriteLine("");
                        }
                    }
                    var reader = new UmbracoConvoy.File.Reader();
                    int recordsRead = reader.Read<Mapper.TestRecord>(filename2, true, Mapper.Process<TestRecord>);
                    Assert.AreEqual(recordsRead, records.Length);
                    recordsRead = reader.Read<Mapper.TestRecord, string>(filename2, true, Mapper.ProcessWithData<TestRecord, string>, format.Item3);
                    Assert.AreEqual(recordsRead, records.Length);
                }
                finally
                {
                    System.IO.File.Delete(filename2);
                }
                
            }
        }
    }
}
